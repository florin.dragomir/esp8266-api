// const express = require('express');
// const app = express(); 
// only for web server wich i am not using rn

// imports
let five = require("johnny-five");
const { Led } = require("johnny-five");

const { EtherPortClient } = require('etherport-client');

const webSocket = require("ws");
const wss = new webSocket.Server({
    port: 81
});

// connection details for the board
const board = new five.Board({
    port: new EtherPortClient({
        host: '192.168.1.150', // esp ip
        port: 3030
    }),
    repl: false
});

/* 
                    esp9266 pinout

                     Wifi Antenna
                 +-----------------+
           --    |                 |    16
           --    |                 |    5
           --    |                 |    4
           10    |                 |    0
            9    |     ESP82666    |    2
           --    |     NODEMCU     |    3.3 V
           --    |                 |    GND
           --    |                 |    14
           --    |                 |    12
          GND    |                 |    13
        3.3 V    |                 |    15
           --    |                 |    RX - 03
           --    |                 |    TX - 01
        3.3 V    |                 |    GND
           --    |                 |    3.3 V
                 |                 |
                 +-----------------+
                      USB PORT


transistor: 2N 2222A
viewing from the flat side: emitter, base, Collector
*/

// interaces
interface stateInterface {
    users: string[];
}
interface welcomeUserInterface {
    remoteAddress: string;
    builtInLed?: any;
}

// state
let state: stateInterface = {
    users: []
}

// functions
const welcomeUser = (data: welcomeUserInterface) => {
    state.users.push(data.remoteAddress);
    console.log(`[${data.remoteAddress}]: has connected`)

    if (data.builtInLed) {
        data.builtInLed.blink();
        setTimeout(() => {
            data.builtInLed.fadeIn();
        }, 1000);
    }
    
}

// start board
board.on('ready', () => {
    const builtInLeds = [
        new Led(2),
        new Led(16)
    ]

    const led_one = new Led(5);

    // turn off  built in led's
    builtInLeds.forEach(led => {
        led.fadeIn();
    });

    wss.on('connection', function (ws: any, req: any) {
        console.log(req);
        welcomeUser({
            remoteAddress: req.connection.remoteAddress,
            builtInLed: builtInLeds[0]
        });

        ws.on('message', function (data: string) {
            /*
                switch on "data"
            */
            if (data === "off") {
                led_one.stop().off();
                console.log(`[${req.connection.remoteAddress}]: turned off`);
            }
            if (data === "blink") {
                led_one.stop().off();
                led_one.blink(500);
                console.log(`[${req.connection.remoteAddress}]: "blink" started`);
            }
            if (data === "anim") {
                led_one.pulse({
                    easing: "linear",
                    duration: 3000,
                    cuePoints: [0, 0.2, 0.4, 0.6, 0.8, 1],
                    keyFrames: [0, 10, 0, 50, 0, 255],
                    onstop() {
                      console.log(`[${req.connection.remoteAddress}]: "anim" stopped`);
                    }
                  });
            }
        });
        ws.on('close', () => {
            console.log(`a user has closed the connection`);
        })
    })
})